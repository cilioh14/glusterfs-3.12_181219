#!/bin/bash


gluster volume stop glusterfs
gluster volume delete glusterfs
umount -l /mnt/glserver1
#umount -l /mnt/glserver2
service glusterd stop

rm /var/log/glusterfs/bricks/*
rm /var/log/glusterfs/cli.log
rm /var/log/glusterfs/glusterd.log

./autogen.sh
./configure
make -j16
make install -j16

systemctl daemon-reload
service glusterd start




#mkfs -t ext4 /dev/sda
#mkfs -t ext4 /dev/sdb
mount -t ext4 /dev/sda /mnt/glserver1
#mount -t ext4 /dev/sdb /mnt/glserver2


#gluster volume create glusterfs transport tcp sdc-node20:/mnt/glserver1/dir1 sdc-node20:/mnt/glserver1/dir2 sdc-node20:/mnt/glserver2/dir1 sdc-node20:/mnt/glserver2/dir2 force
#gluster volume create glusterfs transport tcp sdc-node20:/mnt/glserver1/dir1 sdc-node20:/mnt/glserver1/dir2 sdc-node20:/mnt/glserver2/dir1 sdc-node20:/mnt/glserver2/dir2 force
gluster volume create glusterfs transport tcp sdc-node20:/mnt/glserver1 force
gluster volume set glusterfs performance.io-thread-count 1
gluster volume set glusterfs server.event-threads 1

gluster volume start glusterfs

